<?php 

include("functions.php");
include("config.php");

$item_name = $_GET['item_name']; 
$item_number = $_GET['item_number']; 
$txn_id = $_GET['tx'];
$payment_amount = $_GET['amt'];
$currency_code = $_GET['cc'];
$payment_status = $_GET['st'];
$login = $_GET['cm']; 

$tid = $txn_id;
$auth_token = "v6BJPoal37StCLWKeDsioCAExaiOXXkhdavAwDSp3FjFCWGhSeR9Mq81RnK";
$paypal_url = "www.sandbox.paypal.com";

$url = "https://" . $paypal_url . "/cgi-bin/webscr";

$post_vars = "cmd=_notify-synch&tx=" . $tid . "&at=" . $auth_token;

#$login     = $_POST['cm'];
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post_vars);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 15);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_USERAGENT, 'cURL/PHP');

$fetched = curl_exec($ch);

$lines = explode("\n", $fetched);
$keyarray = array();
if (strcmp ($lines[0], "SUCCESS") == 0) {
	for ($i=1; $i<count($lines);$i++){
		list($key,$val) = explode("=", $lines[$i]);
		$keyarray[urldecode($key)] = urldecode($val);
		//echo urldecode($key) . ":  " . urldecode($val) . "</br>";
	}
	$firstname = $keyarray['first_name'];
	$lastname  = $keyarray['last_name'];
	$itemname  = $keyarray['item_name'];
	$amount    = $keyarray['mc_gross'];
	$currency  = $keyarray['mc_currency'];
	$city      = $keyarray['address_city'];
	$email     = $keyarray['payer_email'];
	$street    = $keyarray['address_street'];
	$option    = $keyarray['option_selection1'];
	$country   = $keyarray['address_country'];
	$receiverEmail = $keyarray['receiver_email'];
	// check that txn_id has not been previously processed
	$valid_txnid = check_txnid($txn_id);

	if (!check_price($amount, $option)) {
			echo ("invalid price.");
			die();
	}

	if (!$valid_txnid) {
			echo ("Your order has already been processed.");
			die();
	}

	// check that receiver_email is your Primary PayPal email
	if ($receiverEmail !== "editor-facilitator@hdcn.net") {
			echo ("<h2>Sorry, something went wrong</h2>");
			die();
	}
	// check that payment_amount/payment_currency are correct
	if ($payment_amount !== $amount) {
			echo ("<h2>Sorry, something went wrong</h2>");
			die();
	}
	if ($currency_code !== $currency) {
			echo ("<h2>Sorry, something went wrong</h2>");
			die();
	}
	// process payment
	$data = [
		'txn_id'			=> $txn_id,
		'payment_amount'    => $amount,
		'payment_status'    => 'completed',
		'item_name'         => $itemname,
		'currency_code'     => $currency,
		'first_name'        => $firstname,
		'last_name'         => $lastname,
		'payer_email'       => $email,
		'address_street'    => $street . ';' . $city ,
		'option_selection1' => $option,
		'address_country'   => $country,
		'receiverEmail'    => $receiverEmail,
		'cm'             => $login 
	];
	$orderid = updatePayments($data);

	mailToBayer($data);
	mailToSeller($data);
	echo ("<h2>Thank you for your purchase!</h2>");

}
else if (strcmp ($lines[0], "FAIL") == 0) {
    echo ("<h2>Sorry, something went wrong</h2>");
}




   
